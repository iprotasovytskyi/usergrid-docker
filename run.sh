#!/usr/bin/env bash

echo "+++ start usergrid"

while [ -z "$(curl -s localhost:8080/status | grep '"cassandraAvailable" : true')" ] ;
do
  echo "+++ tomcat log:"
  tail -n 20 /opt/bitnami/tomcat/logs/catalina.out
  echo "+++ waiting for cassandra being available to usergrid"
  sleep 2
done

echo "+++ usergrid database setup"
curl --user admin:admin -X PUT http://localhost:8080/system/database/setup

echo "+++ usergrid database bootstrap"
curl --user admin:admin -X PUT http://localhost:8080/system/database/bootstrap

echo "+++ usergrid superuser setup"
curl --user admin:admin -X GET http://localhost:8080/system/superuser/setup

echo "+++ create organization and corresponding organization admin account"
curl -D - \
     -X POST  \
     -d "organization=test&username=testadmin&name=testadmin&email=testadmin@example.com&password=testadmin" \
     http://localhost:8080/management/organizations

echo "+++ create admin token with permissions"
export ADMINTOKEN=$(curl -X POST --silent "http://localhost:8080/management/token" -d "{ \"username\":\"testadmin\", \"password\":\"testadmin\", \"grant_type\":\"password\"} " | cut -f 1 -d , | cut -f 2 -d : | cut -f 2 -d \")
echo ADMINTOKEN=$ADMINTOKEN

echo "+++ create app"
curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -H "Content-Type: application/json" \
     -X POST -d "{ \"name\":\"test\" }" \
     http://localhost:8080/management/orgs/test/apps


echo "+++ delete guest permissions"
curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -X DELETE "http://localhost:8080/test/test/roles/guest"

echo "+++ delete default permissions which are too permissive"
curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -X DELETE "http://localhost:8080/test/test/roles/default"


echo "+++ create new guest role"
curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -X POST "http://localhost:8080/test/test/roles" \
     -d "{ \"name\":\"guest\", \"title\":\"Guest\" }"

echo "+++ create new default role, applied to each logged in user"
curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -X POST "http://localhost:8080/test/test/roles" \
     -d "{ \"name\":\"default\", \"title\":\"User\" }"


echo "+++ create guest permissions required for login"
curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -X POST "http://localhost:8080/test/test/roles/guest/permissions" \
     -d "{ \"permission\":\"post:/token\" }"

curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -X POST "http://localhost:8080/test/test/roles/guest/permissions" \
     -d "{ \"permission\":\"post:/users\" }"

curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -X POST "http://localhost:8080/test/test/roles/guest/permissions" \
     -d "{ \"permission\":\"get:/auth/facebook\" }"

curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -X POST "http://localhost:8080/test/test/roles/guest/permissions" \
     -d "{ \"permission\":\"get:/auth/googleplus\" }"

echo "+++ create default permissions for a logged in user"
curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -X POST "http://localhost:8080/test/test/roles/default/permissions" \
     -d "{ \"permission\":\"get,put,post,delete:/users/\${user}/**\" }"

curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -X POST "http://localhost:8080/test/test/roles/default/permissions" \
     -d "{ \"permission\":\"post:/notifications\" }"

echo "+++ create user"
curl -D - \
     -H "Authorization: Bearer ${ADMINTOKEN}" \
     -X POST "http://localhost:8080/test/test/users" \
     -d "{ \"username\":\"testuser\", \"password\":\"testuser\", \"email\":\"testuser@example.com\" }"

echo
echo "+++ done"